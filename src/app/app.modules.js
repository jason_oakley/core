define( function ( require, exports, module ) 
{
    "use strict";

    var Fover   = require( 'base/fover' ),
        modules = require( 'config/modules.config' ).modules;

    var Modules = Fover.Object.extend(
        {
            initialize: function ()
            {
                if ( ! modules ) { return; }
                
                if ( ( _.isObject( modules ) || _.isArray( modules ) ) && _.size( modules ) > 0 )
                {
                    _.each( modules, function ( m ) 
                        {
                            new m();
                        }
                    );
                }
            }

        }
    );

    return Modules;

});
