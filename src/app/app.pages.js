define( function ( require, exports, module ) 
{
    "use strict";

    /**
     * Simple method for loading simple view files that do not need
     * their own router and controller
     * 
     */

    var Fover = require( 'base/fover' ),
        pages = require( 'config/pages.config' ).pages;

    var appController = {
        // Simple View Method
        page : function (arg1)
        {
            var options = {},
                simpleView,
                viewName = this._getFragment();

            // If route is missing from page config
            if ( _.indexOf(_.keys(pages), viewName) === -1 ) {
                simpleView = new pages['missing']['view']({
                    requestedView: viewName
                });
                Fover.region.show(simpleView);
                return;
            }

            // If arguments in route
            if (arguments.length > 0) {
                options.args = arguments;
            }
            // if has collection
            if (pages[viewName]['collection']) {
                options.collection = new pages[viewName]['collection'] ()
                options.collection.fetch();
            }
            // if has model
            if (pages[viewName]['model']) {
                options.model = new pages[viewName]['model'] ()
                // options.model.fetch();
            }

            // initialize the view            
            simpleView = new pages[viewName]['view'] ( options );
            // ...and show it
            Fover.region.show(simpleView);
        },

        anotherpage : function () {
            // called by a route with 'method' option set to 'anotherpage' in page config
        },

        _getFragment: function()
        {
            return Backbone.history.fragment.indexOf( '/' ) ? Backbone.history.fragment.split( '/' )[ 0 ] : Backbone.history.fragment;
        }
    };

    var AppPages = Fover.Object.extend({
        initialize: function()
        {
            var routes = {},
                titles = {};
            _.each(pages, function(p, i) {
                routes[p.route] = p.method || 'page';
                titles[i] = p.title;
            });

            var pageRouter = new Fover.Router();
            pageRouter.titles = titles;
            pageRouter.processAppRoutes(appController, routes)
        }
    });

    return AppPages;

});