define( 'app', function ( require, exports, module ) 
{
    "use strict";

    var Fover   = require( 'base/fover' ),
        Regions = require( 'config/regions.config' ).regions;

    window.Fover = Fover;

    var Base = function ()
    {
        var app;

        // Tell Marionette to use "our" version of jQuery. This
        // will sometimes generate issues when binding to `this.ui`
        // within a view.
        Marionette.View.prototype.$ = jQuery;

        // Define new application structure
        app = new Backbone.Marionette.Application(
            {
                initialize: function ()
                {
                    console.groupCollapsed( 'New Marionette Application Started' );
                    console.log( this );
                    console.groupEnd();
                }
            }
        );

        // Define application regions
        app.addRegions( Regions );

        return app;
    };

    return Base;
});

require([
    'app',
    'app.modules',
    'app.pages'
], function ( app, Modules, Pages ) 
{
    var app     = ( new ( require( 'app' ) )() ),
        Modules = require( 'app.modules' ),
        Pages   = require( 'app.pages' );

    app.pages       = new Pages();
    app.modules     = new Modules();

    app.start();
});