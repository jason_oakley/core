define( function ( require, exports, module ) 
{
    "use strict";

    /**
     * Configuration for app pages which have routes and potentially some collections,
     * but are otherwise not very complicated.
     *
     * Sructure:
     *
     * 'key' = {               // currently this needs to the first part of the route
     * 
     *     'route'      : 'page(/:sub)'                  // backbone route
     *     'title'      : 'My Page',                     // used for <title>
     *     'view'       : require('path/to/view'),       // view or layout
     *     'collection' : require('path/to/collection'), // collection (optional)
     *     'model'      : require('path/to/model'),      // model (optional)
     *     
     * }
     * 
     */

    exports.pages = {

        // '<page_id>' : {
        //     'view'       : require( 'app/pages/<page_id>/views/<page_id>.view' ),
        //     'route'      : '*path',
        // }

    };

});