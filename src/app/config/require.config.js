require.config( {
    paths: {
        // CORE
        // =======================================================
        async                       : '../assets/vendor/requirejs/plugins/async',
        backbone                    : '../assets/vendor/backbone/backbone',
        jquery                      : '../assets/vendor/jquery/jquery',
        handlebars                  : '../assets/vendor/handlebars/handlebars',
        hbh                         : 'helpers/hbh',
        json2                       : '../assets/vendor/json2/json2',
        marionette                  : '../assets/vendor/marionette/backbone.marionette',
        text                        : '../assets/vendor/requirejs/plugins/text',
        underscore                  : '../assets/vendor/underscore/underscore'
    },
    shim: {
        backbone: {
            deps: [
                'underscore',
                'jquery'
            ],
            exports: 'Backbone'
        },

        marionette: {
            deps: [
                'underscore',
                'backbone',
                'jquery'
            ],
            exports: 'Marionette'
        },

        handlebars: {
            exports: 'Handlebars'
        }
    },

    urlArgs : 'buildid=1421777745537',

    baseUrl : '/app',

    deps    : [
        'base'
    ]
});