define( function ( require, exports, module ) 
{
    "use strict";
    
    return {
        
        apiurl                    : "https://dev-api.4over.com",
        environment               : "local",
        productsApiUrl            : "https://dev-api.4over.com",
        productsConfigUrl         : "null",
        fileserviceUrl            : "https://dev-api.4over.com/files?",
        supportTicketsUrl         : "http://dev-supportticket.tools.4over.com",
        uploadUrl                 : "https://uploader-packaging.4over.com/",
        tradeUrl                  : "https://qa-trade.4over.com",
        showFilteredProducts      : "true",
        testOrder                 : "false",
        skipConformation          : "false",
        preflightservice          : "https://dev-preflight.4over.com/preflightservices",
        googleAnalytics           : "UA-2420296-24",
        proxyServer               : "https://dev-api-ultra.4over.com/proxy",
        legacyRegistration        : "false",
        legacyForgotPassword      : "false",
        legacySupportTickets      : "false",
        legacyCallback            : "false",
        packagingEmail            : "jasono@4over.com"
    };

});
