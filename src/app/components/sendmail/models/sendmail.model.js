define( function ( require, exports, module )
{
    "use strict";

    return Fover.Model.extend({
        setUrl: function ( url )
        {
            this.url = url;

            return this;
        }
    });
});