define( function ( require, exports, module )
{
    "use strict";

    var Model = require( 'app/components/sendmail/models/sendmail.model' );

    var Component = Fover.Object.extend(
    {
        url     : null,

        baseUrl : '/formmailer',

        options : {
            to_email        : 'jasono@4over.com',
            from_email      : 'jasono@4over.com',
            subject         : 'Ultra Form Submission',
            company_name    : 'Ultra'
        },

        initialize: function ( options )
        {
            if ( _.isObject( options ) && _.size( options ) > 0 )
            {
                if ( options.url )
                {
                    this.url = options.url + this.baseUrl;
                }
                else
                {
                    throw new Error( 'sendMail needs a URL to send to!' );
                }
            }

            this.model = new Model().setUrl( this.url );

            this.setFrom().setTo().setSubject().setCompany();
        },

        setFrom: function ( str )
        {
            var email = str || this.options.from_email;

            this.model.set({
                from_email: email
            });

            return this;
        },

        setTo: function ( str )
        {
            var email = str || this.options.to_email;

            this.model.set({
                to_email: email
            });

            return this;
        },

        setSubject: function ( str )
        {
            var subject = str || this.options.subject;

            this.model.set({
                subject: subject
            });

            return this;
        },

        setCompany: function ( str )
        {
            var company = str || this.options.company_name;

            this.model.set({
                company_name: company
            });

            return this;
        },

        send: function( data )
        {
            this.model.set( data );

            this.model.save(
                {
                    success : function( model, response ) 
                    {
                        console.info( 'FormMailer Success!', model, response );
                    },

                    error : function( model, response ) 
                    {
                        console.error( 'FormMailer Failure!', model, response );
                    }
                }
            );

            return this.model;
        }
    });

    return Component;
});