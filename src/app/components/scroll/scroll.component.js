/**
 * Smooth Scroll Module
 * Scroll to a target element in style!
 *
 */
define( function ()
{
    var Scroller = function()
    {
        this.go = function( opts )
        {
            var viewport    = $( 'html, body' );

            var target      = opts.target,
                offset      = opts.offset || 0,
                duration    = opts.duration || 1000,
                onComplete  = opts.onComplete || function() {};

            // Animate to target
            viewport.animate({
              scrollTop : $( opts.target ).offset().top - offset
            }, duration, onComplete );

            // Stop the animation if the user scrolls. Defaults on .stop() should be fine
            // viewport.bind( 'scroll mousedown DOMMouseScroll mousewheel keyup', function( e ) {
            //     if ( e.which > 0 || e.type === 'mousedown' || e.type === 'mousewheel' ) {
            //         viewport.stop().unbind( 'scroll mousedown DOMMouseScroll mousewheel keyup' );
            //     }
            // });

            return false;
        };

    };

    return Scroller;
});
