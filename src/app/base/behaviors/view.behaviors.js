define( function ( require, exports, module ) 
{
    "use strict";

    require( 'marionette' );

    var Config = require( 'config/app.config' );

    var ViewBehaviors = Backbone.Marionette.Behavior.extend({

        initialize: function()
        {
            _.extend(this.view, {
                '_proxy' : this._proxy
            });
        },

        onRender: function()
        {
            // This is a hack to make Marrionette views work with old Thorax views
            // which add the view name to data-view-name attribute
            if (this.view.name) {
                this.$el.attr('data-view-name', this.view.name);
            }
        },


        /**
         * Sends a proxy request to the API
         *
         * @param  {string} url  Requested URL
         * @param  {object} opts Proxy options
         * @param  {object} args Callback options
         * @return {object}      Callback response
         */
        _proxy: function( url, opts, args )
        {
            var defaults = {
                    type    : 'POST',
                    data    : {},
                    callback: null
                },
                options = _.extend( defaults, opts );
                args    = _.extend( {}, args )
            ;

            $.ajax({
                type        : options.type,
                url         : Config.proxyServer + '?native=true&url=' + encodeURIComponent( url ) + '&follow=false',
                data        : options.data,
                xhrFields   : {
                    withCredentials: false
                },
            })
            .success( function( data, textStatus, jqXHR )
            {
                if ( options.callback && typeof options.callback === 'function' ) {
                    options.callback( options.data, $.parseJSON( jqXHR.responseText ), args );
                }
            })
            .error(function(jqXHR, textStatus, errorThrown)
            {
                if ( options.callback && typeof options.callback === 'function' ) {
                    options.callback( options.data, $.parseJSON( jqXHR.responseText ), args );
                }
            });
        }

    });

    return ViewBehaviors

});