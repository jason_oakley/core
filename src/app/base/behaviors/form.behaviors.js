define( function ( require, exports, module ) 
{
    "use strict";

    require('marionette');

    var Form = require('base/behaviors/form/form');

    var FormBehaviors = Backbone.Marionette.Behavior.extend({

        initialize: function()
        {
            _.extend(this.view, Form.View);
        }

    });

    return FormBehaviors;

});