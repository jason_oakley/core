define( function ( require, exports, module ) 
{
    "use strict";

    var ViewBehaviors = require('base/behaviors/view.behaviors'),
        FormBehaviors = require('base/behaviors/form.behaviors');

    var BaseItemView = Backbone.Marionette.ItemView.extend({

        behaviors: {
            ViewBehaviors: {
                behaviorClass: ViewBehaviors
            },

            FormBehaviors: {
                behaviorClass: FormBehaviors
            }
        }

    });

    return BaseItemView;

});