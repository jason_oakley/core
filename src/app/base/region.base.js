define( function ( require, exports, module ) 
{
    "use strict";

    require('marionette');

    var region = new Backbone.Marionette.Region({
        el: '#main-page-content'
    });

    return region;
});