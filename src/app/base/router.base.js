define( function ( require, exports, module ) 
{
    "use strict";

    require('marionette');

    var region = require('base/region.base'),
        smoothscroll = require('components/scroll/scroll.component');

    var BaseRouter = Backbone.Marionette.AppRouter.extend({

        default_title : '4over, Inc. | Super Trade Printer',
        titles: {}, // overwrite this

        onRoute: function ( name, path, args ) {
            // region.empty();
            var route = this.__getFragment();
            this.__setBodyAttr( route );
            this.__setTitle( route );
            // Checkin with Google Analytics
            this.__sendPageView( '#/' + route );
            // Scroll to top
            this.scrollTo();
        },

        __getFragment: function()
        {
            return Backbone.history.fragment.indexOf( '/' ) ? Backbone.history.fragment.split( '/' )[ 0 ] : Backbone.history.fragment;
        },

        __setBodyAttr: function( fragment )
        {
            $( 'body' ).attr( 'data-page-template', 'page-' + fragment.replace( /[^a-z0-9.]/g, '' ) );
            return this;
        },

        __setTitle: function ( name )
        {
            var title = this.titles[name] || this.default_title;

            document.title = title;
        },


        /**
         * Sends Google Analytics the page route
         *
         * @param  {string} route Page route URI
         */
        __sendPageView: function( route )
        {
            ga( 'send', 'pageview', route );
        },

        scrollTo: function( elem, offset, duration, callback )
        {
            var $elem       = elem      ? elem      : $( 'body' ),
                $offset     = offset    ? offset    : 200,
                $duration   = duration  ? duration  : 500,
                $callback   = ( ( callback && typeof callback === 'function' ) ? callback : null );

            var scroller    = new smoothscroll();

            scroller.go({
                target      : $elem,
                offset      : $offset,
                duration    : $duration,
                onComplete  : $callback
            });
        },

    });

    return BaseRouter;

});