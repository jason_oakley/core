define( function ( require, exports, module ) 
{
    "use strict";

    require('marionette');

    var BaseCollectionView = Backbone.Marionette.CollectionView.extend({

    });

    return BaseCollectionView;

});