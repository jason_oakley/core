define( function ( require, exports, module ) 
{
    "use strict";

    exports.Object         = require( 'base/object.base' );
    exports.Router         = require( 'base/router.base' );
    exports.ItemView       = require( 'base/itemview.base' );
    exports.LayoutView     = require( 'base/layoutview.base' );
    exports.CollectionView = require( 'base/collectionview.base' );
    exports.Model          = require( 'base/model.base' );
    exports.Collection     = require( 'base/collection.base' );
    exports.region         = require( 'base/region.base' );

});
