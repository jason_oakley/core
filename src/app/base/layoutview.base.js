define( function ( require, exports, module ) 
{
    "use strict";

    require('marionette');

    var FormBehaviors = require('base/behaviors/form.behaviors');

    var BaseLayoutView = Backbone.Marionette.LayoutView.extend({
        FormBehaviors: {
            behaviorClass: FormBehaviors
        }
    });

    return BaseLayoutView;

});