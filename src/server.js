var app         = require( 'express' )(),
    server      = require( 'http' ).Server( app ),
    io          = require( 'socket.io' ).listen( server ),
    bodyParser  = require( 'body-parser' ),
    multer      = require( 'multer' ),
    _           = require( 'underscore' ),
    shelljs     = require( 'shelljs/global' ),
    fs          = require( 'fs' ),
    path        = require( 'path' ),
    gm          = require( 'gm' );

var done = false,
    clientSocket;

var config  = {
    paths   : {
        upload  : './cache/'
    }
};



// CORS
// -------------------------------------
app.all( '*', function( req, res, next )
    {
        res.set({
            'Access-Control-Allow-Origin': 'http://localhost:3000',
            'Access-Control-Allow-Methods': 'GET, POST, OPTIONS, PUT, PATCH, DELETE',
            'Access-Control-Allow-Headers': 'X-Requested-With',
            'Access-Control-Allow-Credentials': 'true'
        });

       next();
    }
);



// SOCKET
// -------------------------------------
io.on( 'connection', function (socket) {
    console.log( 'Socket established with client...' );

    clientSocket = socket;
});



// SERVER
// -------------------------------------
server.listen( 4000, function ()
{

    console.log( 'Listening for uploads on port 4000' );
});