var gulp    = require( 'gulp' );

var plumber     = require( 'gulp-plumber' );
var concat      = require( 'gulp-concat' );
var minify      = require( 'gulp-minify' );
var uglify      = require( 'gulp-uglify' );
var _           = require( 'underscore' );

var environment = 'local';
var paths       = {
    src     : './src/app/',
    dest    : './src/assets/js/',
    vendor  : './src/assets/vendor/',
    assets  : './src/assets/'
};

if ( environment === 'production' )
{
    var paths = _.extend( paths, {
        dest: './dist'
    });
}


// Set Environment
// ===========================================================
gulp.task( 'set-environment', function ( env )
    {
        environment = env;
    }
);

// Clean
// ===========================================================
var del = require( 'del' );

gulp.task( 'clean', function ()
    {
        return del( [ 'dist', 'src/assets/css', 'src/assets/js' ], function ( err, deletedFiles )
            {
                if ( deletedFiles )
                {
                    console.log( 'Files deleted:', deletedFiles.join( ', ' ) );
                }
            }
        );
    }
);

// Copy
// ===========================================================
gulp.task( 'copy', function ()
    {
        return gulp.src( './src/*.html' )
            .pipe( gulp.dest( 'dist' ) );
    }
);

// BrowserSync
// ===========================================================
var browserSync = require( 'browser-sync' );
var reload      = browserSync.reload;

gulp.task( 'browser-sync', function ()
    {
        browserSync({
            notify: false,
            port    : 5309,
            server: {
                baseDir : [ './src', './cache' ]
            }
        });
    }
);

// Sass
// ===========================================================
var sass        = require( 'gulp-sass' );
var sourcemaps  = require( 'gulp-sourcemaps' );

gulp.task( 'sass', function ()
    {
        gulp.src( './src/sass/*.scss' )
            .pipe( sourcemaps.init() )
                .pipe( sass({
                    errLogToConsole: true,

                    // sync: true,
                    onSuccess: function ( result )
                    {
                        console.log( 'Compiled Sass files' );
                    },
                    onError: function ( err )
                    {
                        console.log( 'Somethingdonebrokeohno!' );
                        console.log( err );
                    }
                }))
                // .pipe( sourcemaps.write( '/' ) )
            .pipe( plumber() )
            .pipe( gulp.dest( './src/assets/css' ) )
            .pipe( reload({
                stream: true
            }) );
    }
);

// JSHint
// ===========================================================
var jshint  = require( 'gulp-jshint' );
var stylish = require( 'jshint-stylish' );

gulp.task( 'jshint', function ()
    {
        gulp.src( './src/app/*.js' )
            .pipe( plumber() )
            .pipe( jshint() )
            .pipe( jshint.reporter( stylish) )
            .pipe( jshint.reporter( 'fail' ) );
    }
);

// Node Server
// ===========================================================
// var nodemon = require( 'gulp-nodemon' );

// gulp.task( 'server', function ()
//     {
//         nodemon({
//             script  : 'src/server.js',
//             // ext     : 'html js',
//             // ignore  : [ 'ignored.js' ]
//         })
//         .on( 'change', [ 'jshint'] )
//         .on( 'restart', function ()
//             {
//                 console.log('restarted!')
//             }
//         );
//     }
// );

// Browser Reload
// ===========================================================
gulp.task( 'bs-reload', function ()
    {
        browserSync.reload();
    }
);

// Image Minification
// ===========================================================
// var imagemin    = require( 'gulp-imagemin' );
// var pngquant    = require( 'imagemin-pngquant' );
// var jpegtran    = require( 'imagemin-jpegtran' );
// var gifsicle    = require( 'imagemin-gifsicle' );
// var svgo        = require( 'imagemin-svgo' );

// gulp.task( 'imagemin', function ()
//     {
//         // These can be combine into a single call.
//         // I broke them out in this fashion to test
//         // the Gulp plugin features.

//         // PNG
//         gulp.src( './src/assets/tests/img/png/*' )
//             .pipe( imagemin({
//                 optimizationLevel   : 3,
//                 use                 : [ pngquant() ]
//             }) )
//             .pipe( gulp.dest( 'dist/assets/img/png' ) );

//         // JPG
//         gulp.src( './src/assets/tests/img/jpg/*' )
//             .pipe( imagemin({
//                 progressive : true,
//                 use         : [ jpegtran() ]
//             }) )
//             .pipe( gulp.dest( 'dist/assets/img/jpg' ) );

//         // GIF
//         gulp.src( './src/assets/tests/img/gif/*' )
//             .pipe( imagemin({
//                 interlaced  : true,
//                 use         : [ gifsicle() ]
//             }) )
//             .pipe( gulp.dest( 'dist/assets/img/gif' ) );

//         // SVG
//         gulp.src( './src/assets/tests/img/svg/*' )
//             .pipe( imagemin({
//                 svgoPlugins : [{
//                     removeViewBox: false
//                 }],
//                 use         : [ svgo() ]
//             }) )
//             .pipe( gulp.dest( 'dist/assets/img/svg' ) );
//     }
// );

// Vendor JavaScript
// ===========================================================
// gulp.task( 'vendor-js', function ()
//     {
//         stream = gulp.src([
//             paths.vendor + 'jquery/jquery.js',
//             paths.vendor + 'underscore/underscore.js',
//             paths.vendor + 'backbone/backbone.js',
//             paths.vendor + 'marionette/backbone.marionette.js'
//         ])
//         .pipe( plumber() )
//         .pipe( concat( 'vendor.js' ) )

//         if ( environment === 'production' )
//         {
//             stream.pipe( uglify() )
//         }

//         stream.pipe( gulp.dest( paths.dest ) )
//     }
// );


// Default Task
// ===========================================================
gulp.task( 'default', [ 'clean', 'sass', 'browser-sync' ], function ()
    {
        gulp.watch( 'src/sass/*.scss', [ 'sass' ] );
        gulp.watch( 'src/app/*.js', [ 'jshint', browserSync.reload ] );
        gulp.watch(' src/*.html', [ 'bs-reload' ] );
    }
);
